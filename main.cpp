#include <opencv2/opencv.hpp>
#include <opencv2/face/facemark.hpp>
#include <iostream>
#include <fstream>
#include <string> 

using namespace cv;
using namespace std;

// These variables are used by both thingies that will be added by the user.

// This part here handles the addition of an image to the database to
void AddFaceLandmarks(string namey)
{
    CascadeClassifier face_cascade;
    
    face_cascade.load("./lbpcascade_frontalface_improved.xml");//lbpcascade_frontalface_improved//haarcascade_frontalface_default
    Mat img = imread("./img/" + namey + ".jpg");
    Ptr<cv::face::Facemark> facemark = cv::face::createFacemarkKazemi();
    facemark->loadModel("./face_landmark_model.dat");
    cout << "Loaded model" << endl;

    ofstream outputfile;
    outputfile.open("./img/" + namey + "_landmarks.txt");

    //resize(img, img, Size(460, 460), 0, 0, INTER_LINEAR_EXACT);
    Mat gray;
    std::vector<Rect> faces;
    if (img.channels() > 1) {
        cvtColor(img, gray, COLOR_BGR2GRAY);
    }
    else {
        gray = img.clone();
    }
    cv::equalizeHist(gray, gray);
    face_cascade.detectMultiScale(gray, faces, 1.1, 5, 0, Size(70, 70));

    vector< vector<Point2f> > shapes;
    if (facemark->fit(img, faces, shapes))
    {
        if (faces.size() < 1) {
            std::cout << "No faces were found!" << std::endl;
        }
        else {
            for (size_t i = 0; i < faces.size(); i++)
            {
                cv::rectangle(img, faces[i], Scalar(255, 0, 0));
            }
            for (size_t i = 0; i < faces.size(); i++) {
                for (size_t k = 0; k < shapes[i].size(); k++) {
                    cv::circle(img, shapes[i][k], 5, cv::Scalar(0, 0, 255), FILLED);
                    outputfile << (int)shapes[i][k].x << " " << (int)shapes[i][k].y << endl;
                }
            }
        }

       //namedWindow("Detected_shape");
        //imshow("Detected_shape", img);
    }

    //waitKey(0);
    //destroyAllWindows();
}


//Read points from text file
vector<Point2f> readPoints(string pointsFileName) {
    vector<Point2f> points;
    ifstream ifs(pointsFileName.c_str());
    float x, y;
    int count = 0;
    while (ifs >> x >> y)
    {
        points.push_back(Point2f(x, y));

    }

    return points;
}

// Apply affine transform calculated using srcTri and dstTri to src
void applyAffineTransform(Mat& warpImage, Mat& src, vector<Point2f>& srcTri, vector<Point2f>& dstTri)
{
    // Given a pair of triangles, find the affine transform.
    Mat warpMat = getAffineTransform(srcTri, dstTri);

    // Apply the Affine Transform just found to the src image
    warpAffine(src, warpImage, warpMat, warpImage.size(), INTER_LINEAR, BORDER_REFLECT_101);
}


// Calculate Delaunay triangles for set of points
// Returns the vector of indices of 3 points for each triangle
static void calculateDelaunayTriangles(Rect rect, vector<Point2f>& points, vector< vector<int> >& delaunayTri) {

    // Create an instance of Subdiv2D
    Subdiv2D subdiv(rect);

    // Insert points into subdiv
    for (vector<Point2f>::iterator it = points.begin(); it != points.end(); it++)
        subdiv.insert(*it);

    vector<Vec6f> triangleList;
    subdiv.getTriangleList(triangleList);
    vector<Point2f> pt(3);
    vector<int> ind(3);

    for (size_t i = 0; i < triangleList.size(); i++)
    {
        Vec6f t = triangleList[i];
        pt[0] = Point2f(t[0], t[1]);
        pt[1] = Point2f(t[2], t[3]);
        pt[2] = Point2f(t[4], t[5]);

        if (rect.contains(pt[0]) && rect.contains(pt[1]) && rect.contains(pt[2])) {
            for (int j = 0; j < 3; j++)
                for (size_t k = 0; k < points.size(); k++)
                    if (abs(pt[j].x - points[k].x) < 1.0 && abs(pt[j].y - points[k].y) < 1)
                        ind[j] = k;

            delaunayTri.push_back(ind);
        }
    }

}


// Warps and alpha blends triangular regions from img1 and img2 to img
void warpTriangle(Mat& img1, Mat& img2, vector<Point2f>& t1, vector<Point2f>& t2)
{

    Rect r1 = boundingRect(t1);
    Rect r2 = boundingRect(t2);

    // Offset points by left top corner of the respective rectangles
    vector<Point2f> t1Rect, t2Rect;
    vector<Point> t2RectInt;
    for (int i = 0; i < 3; i++)
    {

        t1Rect.push_back(Point2f(t1[i].x - r1.x, t1[i].y - r1.y));
        t2Rect.push_back(Point2f(t2[i].x - r2.x, t2[i].y - r2.y));
        t2RectInt.push_back(Point(t2[i].x - r2.x, t2[i].y - r2.y)); // for fillConvexPoly

    }

    // Get mask by filling triangle
    Mat mask = Mat::zeros(r2.height, r2.width, CV_32FC3);
    fillConvexPoly(mask, t2RectInt, Scalar(1.0, 1.0, 1.0), 16, 0);

    // Apply warpImage to small rectangular patches
    Mat img1Rect;
    img1(r1).copyTo(img1Rect);

    Mat img2Rect = Mat::zeros(r2.height, r2.width, img1Rect.type());

    applyAffineTransform(img2Rect, img1Rect, t1Rect, t2Rect);

    multiply(img2Rect, mask, img2Rect);
    multiply(img2(r2), Scalar(1.0, 1.0, 1.0) - mask, img2(r2));
    img2(r2) = img2(r2) + img2Rect;
}

int main(int argc, char** argv)
{
    //Read input images
    string filename1 = "vladimir_putin";  //hillary_clinton
    string filename2 = "joe_biden";
    
    ifstream ifile;
    
    
    start1:
    std::cout << "Enter first picture name(without extensions, only filename)" << endl; 
    std::cin >> filename1;
    ifile.open("./img/" + filename1 + ".jpg");   
    if(!ifile)
	{
		std::cout << "\nЗащи́та от дурака́" << endl;
		goto start1;
	}
	ifile.close();
    
    start2:
    std::cout << "\nEnter second picture name(without extensions, only filename)" << endl; 
    std::cin >> filename2;
    ifile.open("./img/" + filename2 + ".jpg");
    if(!ifile)
	{
		std::cout << "\nЗащи́та от дурака́" << endl;
		goto start2;
	}
    
    
    // This here is called Smarts
    AddFaceLandmarks(filename1);
    AddFaceLandmarks(filename2);
    
    // ... or being lazy idk
    Mat img1 = imread("./img/" + filename1 + ".jpg");
    Mat img2 = imread("./img/" + filename2 + ".jpg");
    Mat img1Warped = img2.clone();

    cv::Mat img1_landmarks = img1.clone();


    cv::imshow("Face from", img1);
    cv::imshow("Face to", img2);

    //Read points
    vector<Point2f> points1, points2;
    points1 = readPoints("./img/" + filename1 + "_landmarks.txt");
    points2 = readPoints("./img/" + filename2 + "_landmarks.txt");

    //convert Mat to float data type
    img1.convertTo(img1, CV_32F);
    img1Warped.convertTo(img1Warped, CV_32F);


    // Find convex hull
    vector<Point2f> hull1;
    vector<Point2f> hull2;
    vector<int> hullIndex;

    convexHull(points2, hullIndex, false, false);

    for (int i = 0; i < hullIndex.size(); i++)
    {
        hull1.push_back(points1[hullIndex[i]]);
        hull2.push_back(points2[hullIndex[i]]);
    }


    // Find delaunay triangulation for points on the convex hull
    vector< vector<int> > dt;
    Rect rect(0, 0, img1Warped.cols, img1Warped.rows);
    calculateDelaunayTriangles(rect, hull2, dt);

    // Apply affine transformation to Delaunay triangles
    for (size_t i = 0; i < dt.size(); i++)
    {
        vector<Point2f> t1, t2;
        // Get points for img1, img2 corresponding to the triangles
        for (size_t j = 0; j < 3; j++)
        {
            t1.push_back(hull1[dt[i][j]]);
            t2.push_back(hull2[dt[i][j]]);
        }

        warpTriangle(img1, img1Warped, t1, t2);

    }

    // Calculate mask
    vector<Point> hull8U;
    for (int i = 0; i < hull2.size(); i++)
    {
        Point pt(hull2[i].x, hull2[i].y);
        hull8U.push_back(pt);
    }

    Mat mask = Mat::zeros(img2.rows, img2.cols, img2.depth());
    fillConvexPoly(mask, &hull8U[0], hull8U.size(), Scalar(255, 255, 255));

    // Clone seamlessly.
    Rect r = boundingRect(hull2);
    Point center = (r.tl() + r.br()) / 2;

    Mat output;
    img1Warped.convertTo(img1Warped, CV_8UC3);
    imshow("Just warped", img1Warped);
    seamlessClone(img1Warped, img2, mask, center, output, NORMAL_CLONE);

    imshow("Face Swapped", output);

    //image with landmarks
    for (int i = 0; i < points1.size(); i++) {
        cv::circle(img1_landmarks, cv::Point(points1[i].x, points1[i].y), 3, cv::Scalar(0, 0, 255), -1);
    }

    cv::imshow("img1 landmarks", img1_landmarks);

    //

    waitKey(0);
    destroyAllWindows();


    return 1;
}




